# Memento DWWM4

Dans le cadre de la formation WEB, un ensemble de points techniques sont importants, ce dépôt à pour but d'agrémenter un ensemble de notions sur lesquelles il est possible de s'appuyer pour avoir en tête les points clés de chacune des compétences à travailler :


## Développer la partie front-end d’une application web ou web mobile sécurisée

- [Installer et configurer son environnement de travail en fonction du projet web ou web mobile](/documentation/1-config-working-env.md).
- [Maquetter des interfaces utilisateur web ou web mobile](/documentation/2-maquettage.md).
- [Réaliser des interfaces utilisateur statiques web ou web mobile](/documentation/3-interface-statique.md).
- [Développer la partie dynamique des interfaces utilisateur web ou web mobile](/documentation/4-interface-dynamique.md).

## Développer la partie back-end d’une application web ou web mobile sécurisée

- [Mettre en place une base de données relationnelle](/documentation/5-db.md).
- [Développer des composants d’accès aux données SQL et NoSQL](/documentation/6-composants-db.md).
- [Développer des composants métier coté serveur](/documentation/7-composants-metier.md).
- [Documenter le déploiement d’une application dynamique web ou web mobile](/documentation/8-deploy.md).

