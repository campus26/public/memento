[Retour](/README.md)

# Développer la partie back-end d’une application web ou web mobile sécurisée

## Mettre en place une base de données relationnelle
La création de la base de données relationnelle consiste à établir un plan en fonction des SPECIFICATIONS TECHNIQUES du PROJET. A partir de ce document, il est possible de décliner en BASE DE DONNÉES les informations. Cette DB va permettre de stocker l'ensemble des informations liées au PROJET.

### Conception

- MCD : Le Modèle de CONCEPTION de DONNÉES décrit l'ensemble des ENTITÉS du PROJET ainsi que les relations entre elles. Cette relation est généralement définie par un verbe/ une action entre les deux.

MLD / MPD : Déclinaison du MCD permettant de connaitre toutes les tables et les propriétés associées de la base de données. 

- CARDINALITÉ : C'est la notion permettant de définir la quantité entre deux ENTITÉS (Ex : Un article appartient à une ou plusieurs  catégories )

TABLE : Structure de données qui va contenir les données d'une Entité

PROPRIÉTÉS : Dans une TABLE la propriété correspond à une des caractéristiques de l'ENTITÉ. La notion s'approchant le plus de propriétés serait la colonne d'un tableau.

TUPLES : Enregistrement dans une table, se rapproche de la notion d'instance de CLASS en POO.

CLÉ PRIMAIRE : Identifiant Unique (PK) d'un TUPLES (d'un enregistrement)

CLÉ ÉTRANGÈRE : Identifiant (FK) se trouvant dans une table en lien avec une Clé Primaire d'une autre TABLE.


### SQL 

SELECT : ...

INSERT : ...

UPDATE : ...

DELETE : ...

WHERE : ...

ORDER BY : ...

