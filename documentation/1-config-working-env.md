
[Retour](/README.md)

# Développer la partie front-end d’une application web ou web mobile sécurisée

## Installer et configurer son environnement de travail en fonction du projet web ou web mobile

### GIT
Git est un outil permettant de suivre le cycle de vie d'une application et toutes les évolutions associées.

- VERSIONNING : Le fait de garder en historique la totalité du programme grâce à un ensemble de Commit
- DEPOT / REPO : Espace permettant de gérer le projet avec GIT
- BRANCHE / BRANCH : Dans le cadre d'un projet, il est possible de faire évoluer le programme en faisant une copie temporaire du projet qui évoluera de manière distincte des autres branches.
- COMMI : Sur une branch, il est possible de gérer un ensemble de modification, étape par étape, que l'on appelle "Commit"
- PUSH / MERGE / REBASE : ....

### VISUAL STUDIO CODE
Editeur de texte spécialisé dans l'édition du code source d'une application.

- EXTENSION : Module complémentaire à VSCode permettant de personnaliser un ensemble de fonctionnalité native

### LES TERMINAUX 
Un terminal permet de lancer des programmes en ligne de commande. Dans le cadre d'un projet de création d'application ou de site internet, il existe des outils permettant de faciliter les développements.

- CLI : Ensemble de commandes permettant de faciliter l'initialisation d'un projet, la génération de code, le démarrage ou la compilation d'un projet.
- BASH : Terminal de commandes UNIX, permettant d'éxécuter un ensemble de commande (cd, mv, ls, ... ). Il est utilisé dans la plupart des projets web.
- POWERSHELL : Terminal de commandes WINDOWS, il est spécifique à l'OS Windows.


### PACKAGE MANAGER
Un package manager est un programme généralement en ligne de commandes, permettant de gérer l'installation des librairies, dépendances nécessaire à un projet.

- NPM / NPX : Node Package Manager lié au langage JavaScript, il va initialiser ou mettre à jour un dossier à la racine du projet portant le nom "node_module"
- COMPOSER : Package Manager lié au langage PHP, il va initialiser ou mettre à jour un dossier à la racine du projet portant le nom "vendor"
- APT : Package Manager lié au système d'exploitation UNIX/DEBIAN
- SCOOP : Package Manager lié au système d'exploitation WINDOWSDEBIAN
- BREW : Package Manager lié au système d'exploitation MACOS


