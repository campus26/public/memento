[Retour](/README.md)

# Développer la partie back-end d’une application web ou web mobile sécurisée

## Documenter le déploiement d’une application dynamique web ou web mobile

### Déploiement des applications

Le déploiement d'une application consiste à mettre en place une procédure permettant de gérer la mise en ligne d'un programme et/ou projet.

- FTP : Protocole Obsolète :) permettant de mettre en ligne un projet

- SERVEUR SQL : Serveur permettant généralement de stocker les données d'un site internet, généralement en MYSQL ou MARIADB

- PMA : PhpMyAdmin est un outil / une interface visuelle permettant d'administrer une Base de Données au format MYSQL/MARIADB

- HEBERGEUR : Prestataire s'occupant de gérer le stockage et la mise à disposition de service ( site internet / db / ... ) 

- NOM DE DOMAINE : un NDD est un identifiant unique composé d'un DOMAINE et d'une EXTENSION (domain.tld / example.com) permettant de faire le lien entre le serveur d'hébergement ayant une adresse IP. Il évite à l'utilisateur de devoir passer par l'IP complexe et simplifie l'accès au service.

- ZONE DNS : La Zone DNS permet de définir les différents services en lien avec le DOMAINE. Elle se paramètre sur l'interface du fournisseur de NDD. Il existe plusieurs type d'enregistrement sur la zone DNS (A pour pointer une IP, CNAME pour faire un raccourci ver un autre DOMAIN, MX pour définir les serveurs mails ... )