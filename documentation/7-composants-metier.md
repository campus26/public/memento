[Retour](/README.md)

# Développer la partie back-end d’une application web ou web mobile sécurisée

## Développer des composants métier coté serveur

Consiste à mettre en place des traitements de données côté serveur peu importe la provenance (API / DB / FORMULAIRE ... ), ou de mettre en place des fonctionnalités spécifiques (génération de PDF, QRCODE, traitement d'images, ... )


### DIVERS

- LIBRAIRIES : Ensemble de fonctions et/ou méthode permettant de faciliter le développement.

- COMPOSER / NPM : Package Manager permettant d'automatiser l'installation de LIBRAIRIE

- NODE : ENVIRONNEMENT de DEVELOPPEMENT permettant de créer des applications JAVASCRIPT côté SERVEUR

- FORMULAIRE : ELEMENT permettant de transmettre des données à un composant métier qui ensuite s'occupera de traiter l'information en fonction de l'objectif. Par exemple un Formulaire de CONTACT pourra dans le cadre du code d'un COMPOSANT METIER soit "envoyer un mail avec les données du formulaire" ou/et "stocker les données du formulaire de CONTACT dans une base de données"

### Programmation Orientée Objet

- CLASS : Ensemble de functions et de variables permettant de créer des objets. 

- OBJET  : Un objet est une instance de CLASS

- INSTANCE : L'instance est le fait de créer un Objet à partir d'une CLASS. On parle alors d'instanciation.

- PROPRIÉTÉ : Une variable dans une CLASS

- METHODE : Une function dans une CLASS
---
- PUBLIC/PROTECTED/PRIVATE : Dans le cadre de la POO, il est possible de définir des droits d'accès aux PROPRIÉTÉS et aux METHODES.

- PUBLIC : PROPRIÉTÉS / METHODES accessibles partout

- PROTECTED : PROPRIÉTÉS / METHODES accessibles dans une class héritée.

- PRIVÉ : PROPRIÉTÉS / METHODES accessibles dans la CLASS.
---

- FUNCTION : On pourrait définir une fonction comme une boite dans laquelle on pourrait insérer des éléments (arguments), en secouant la boîte celle-ci nous permettrait d'obtenir un élément final. 
Une function permet d'éxécuter un ensemble d'instructions. Elle prend généralement des arguments en paramètre, et retourne le résultat du traitement effectué par les instructions.

- HERITAGE : L'héritage consiste à prendre une class "PARENT", et à implémenter ses méthodes et ses propriétés pour spécifier des actions plus précise. Exemple une class VOITURE pourrait hériter d'une CLASS VEHICULE.

PROMISE : Notion en JS qui consiste à éxécuter un code ASYNCHRONE. Une PROMISE va prendre en paramètre les function de REUSSITE ou d'ECHEC.


