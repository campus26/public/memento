[Retour](/README.md)

# Développer la partie back-end d’une application web ou web mobile sécurisée

## Développer des composants d’accès aux données SQL et NoSQL

Consite à mettre en place un code dans un langage dynamique permettant d'interagir avec une base de données.


### Divers

CRUD : Opération élémentaire permettant d'administrer une TABLE / ENTITÉ

SQL : Langage permettant de communiquer avec une BASE de DONNÉES RELATIONNELLE 

PDO : Librairie PHP permettant de manipuler des DB.

ORM : Librairie (PHP/JS/...) permettant de manipuler des données stockées dans des DB évitant d'utiliser le langage SQL en créant des CLASS d'abstraction entre le PROGRAMME et la DB.

POSTMAN / THUNDERCLIENT : Outils permettant de tester des APIS en interrogeant des ROUTES avec différentes METHOD (GET/POST/DELETE/...)
