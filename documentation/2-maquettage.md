[Retour](/README.md)

# Développer la partie front-end d’une application web ou web mobile sécurisée

## Maquetter des interfaces utilisateur web ou web mobile


### Notions Génériques 

- IMAGE BITMAP : Image définie dans un tableau par des pixels (JPG, PNG)
- IMAGE VECTORIELLE : Image défini par des formules mathématique (SVG)
- UX : Interface Mockup, très simplifié permettant de positionner les différents élements de l'interface web/mobile
- UI : L'UI consiste généralement à styliser un mockup après validation de la disposition des éléments présent dans l'UX
- CHARTE GRAPHIQUE : La charte graphique consiste à définir les lignes directives des choix esthétiques et graphique d'un projet.
(Couleurs / typographie / Logo ...)

### Logiciels

- Figma : Outils permettant de créer des MAQUETTES en fonction des supports cibles. Cet outil collaboratif permet d'adapter dynamiquement sa maquette grâce à des outils de PAO intégré
- Adobe XD : Même principe que Figma
- Photoshop : Logiciel de retouche d'images "bitmap"
- Illustrator : Permet de créer des images "vectorielles"


### Ergonomie

- RESPONSIVE : Adaptation de l'affichage d'une interface en fonction du terminal cible (mobile, tablette, pc, tv ...)
- ACCESSIBILITE : Consiste à réfléchir et adapter les interfaces pour qu'elles puissent être accessible aux utiliseurs en situation de handicap (visuel/auditif/...)

