[Retour](/README.md)

# Développer la partie front-end d’une application web ou web mobile sécurisée

## Développer la partie dynamique des interfaces utilisateur web ou web mobile

Consiste à transformer une interface statique faite en HTML / CSS, et à intégrer des éléments dynamiques grâce à des données extérieures. On pourrait prendre comme exemple une page ARTICLE.HTML statique, qui serait remplacée en fonction de l'URL de l'application par les données d'une API ou d'une base de données, ou d'un fichier quelconque.


### Divers 
LANGAGES DYNAMIQUES: Langage de programmation permettant d'intégrer des DATAS dans un DESIGN HTML/CSS (JAVASCRIPT / PHP / ... )

API : Base de données externe utilisable par le protocole HTTP (en général), la norme la plus courante pour appeler une API et la norme REST

REST : Norme pour appeler une API passant généralement par la ressource, et permettant de faire un CRUD en HTTP (POST/GET/PUT/DELETE)

LOCAL STORAGE : Stockage Local dans le navigateur

ROUTE : La notion de ROUTE évoque une URL permettant d'éxécuter une action spécifique dans un programme pouvant utiliser des paramètres provenant de l'URL.

...