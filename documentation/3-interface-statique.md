[Retour](/README.md)

# Développer la partie front-end d’une application web ou web mobile sécurisée

## Réaliser des interfaces utilisateur statiques web ou web mobile

L'interface statique d'un site internet consite à transformer une maquette FIGMA/XD ... en code HTML / CSS.

### HTML

- BALISE : Une balise HTML permet de structurer les informations que l'on affiche sur une page web. Il existe globalement deux groupes, inline (span, b, a) et block (div, h1, p).

- ATTRIBUT : Un attribut permet de personnaliser une balise. Par exemple l'attribut CLASS va pouvoir nommer la balise pour ensuite choisir de la personnaliser dans le CSS


### CSS

- SELECTEUR : dans le cadre du code CSS, le sélecteur permet de cibler une ou plusieurs balises pour ensuite leur appliquer une ou plusieurs propriétés.
- PROPRIÉTÉ : Les propriétés vont permettre d'appliquer des élements de style (couleur/typo...) aux balises ciblées par le sélecteur.
- CODE COULEUR : Il existe plusieurs manières différentes de définir une couleur en CSS (Hexadécimal, rgb, rgba ...)
- DISPLAY : la notion de display est liée généralement à la disposition que l'on souhaite des éléments en rapport avec les éléments ciblés. (inline, block, flex, grid, none)
- SASS : Compilateur permettant de dynamiser le CSS en intégrant des notions de variables, et d'imbrication (parent/enfant) de SELECTEUR 

